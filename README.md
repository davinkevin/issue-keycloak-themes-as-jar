# issue-keycloak-themes-as-jar

## Issue with jar format templates

To build the app: 

```shell
$ mvn -f themes-example/ clean package
[INFO] Scanning for projects...
[INFO]
[INFO] ----------------< org.keycloak:keycloak-example-themes >----------------
[INFO] Building Themes Examples 1.0.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ keycloak-example-themes ---
[INFO]
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ keycloak-example-themes ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] Copying 23 resources
[INFO]
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ keycloak-example-themes ---
[INFO] No sources to compile
[INFO]
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ keycloak-example-themes ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /Users/kevin/workspace/gitlab.com/davinkevin/issue-keycloak-themes-as-jar/themes-example/src/test/resources
[INFO]
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ keycloak-example-themes ---
[INFO] No sources to compile
[INFO]
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ keycloak-example-themes ---
[INFO] No tests to run.
[INFO]
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ keycloak-example-themes ---
[INFO] Building jar: /Users/kevin/workspace/gitlab.com/davinkevin/issue-keycloak-themes-as-jar/themes-example/target/keycloak-example-themes.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.040 s
[INFO] Finished at: 2020-12-07T07:57:11+01:00
[INFO] ------------------------------------------------------------------------
$ docker build -f ./themes-example/src/docker/as-jar/Dockerfile -t keycloak-issue themes-example/
Sending build context to Docker daemon    1.4MB
Step 1/4 : FROM jboss/keycloak:11.0.3
11.0.3: Pulling from jboss/keycloak
4753a4528f5f: Already exists
c0194df27eff: Already exists
8709b125b22d: Pull complete
4fdf05969919: Pull complete
0e2a010a5ecc: Pull complete
Digest: sha256:5242f5e0d7b22b9947c275f6ad9f92d7e9a870540cc6a6042b85185115e1af2f
Status: Downloaded newer image for jboss/keycloak:11.0.3
 ---> 0d204cc62b07
Step 2/4 : COPY --chown=jboss:jboss target/keycloak-example-themes.jar /tmp
 ---> 09e06ede2265
Step 3/4 : RUN /opt/jboss/keycloak/bin/jboss-cli.sh --command="module add --name=org.keycloak.example.themes --resources=/tmp/keycloak-example-themes.jar"
 ---> Running in 4a938a94733b
Removing intermediate container 4a938a94733b
 ---> d5519855c18a
Step 4/4 : COPY --chown=jboss:jboss standalone.xml /opt/jboss/keycloak/standalone/configuration/
 ---> 91c80394cde7
Successfully built 91c80394cde7
Successfully tagged keycloak-issue:latest
```

After that build process, we can launch keycloak: 

```shell
$ docker run -it --rm -e KEYCLOAK_USER=foo -e KEYCLOAK_PASSWORD=foo -e DB_ADDR=localhost -e DB_VENDOR=h2 -p 8080:8080 keycloak-issue
```

We can see the template is not available in the Keycloak UI.

![not working with jar](img/as-jar.png)

## File copy are working

To build the app:

```shell
$ docker build -f ./themes-example/src/docker/as-files/Dockerfile -t keycloak-issue-with-files themes-example/
```

After that build process, we can launch keycloak:

```shell
$ docker run -it --rm -e KEYCLOAK_USER=foo -e KEYCLOAK_PASSWORD=foo -e DB_ADDR=localhost -e DB_VENDOR=h2 -p 8080:8080 keycloak-issue-with-files
```

We can see the template are present: 

![working with files](img/as-files.png)
